import numpy as np


class Metric:
    name = 'metric'

    def __call__(self, y1, y2):
        raise NotImplementedError


class Accuracy(Metric):
    name = 'accuracy'

    def __call__(self, y1, y2):
        y1 = np.asarray(y1)
        y2 = np.asarray(y2)

        if len(y1.shape) > 1:
            y1 = np.argmax(y1, axis=1)
        if len(y2.shape) > 1:
            y2 = np.argmax(y2, axis=1)

        return np.mean(y1 == y2)
