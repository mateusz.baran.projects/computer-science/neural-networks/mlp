import numpy as np
import matplotlib.pyplot as plt


def plot(y_list, labels, x_label='', y_label='', title=''):
    fig, ax = plt.subplots(figsize=(10, 5))
    fig.canvas.set_window_title(title)

    ax.grid(True)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    for y, label in zip(y_list, labels):
        x = np.arange(0, len(y))
        ax.plot(x, y, label=label)
    ax.legend()


def plot_bar(y_list, labels, x_label='', y_label='', title=''):
    fig, ax = plt.subplots(figsize=(10, 5))
    fig.canvas.set_window_title(title)

    ax.yaxis.grid(True)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.bar(labels, y_list)


def val_accuracy_of_epoch(results, title=''):
    y_list = results['val_accuracy']
    labels = results['label']
    plot(y_list, labels, 'epochs', 'accuracy', title)


def accuracy_and_val_accuracy_of_epoch(results, title=''):
    y_list_1 = results['accuracy']
    y_list_2 = results['val_accuracy']
    y_list = zip(y_list_1, y_list_2)
    labels = results['label']

    for y, label in zip(y_list, labels):
        plot(y, ('train', 'validation'), 'epochs', 'accuracy', f'{title} {label}')


def time_of_experiment(results, title=''):
    x_label = results['label'][0].split('=')[0]
    y_list = results['time']
    labels = [l.split('=', 1)[1] for l in results['label']]
    plot_bar(y_list, labels, x_label, 'time', title)


def show_plots():
    plt.show()
