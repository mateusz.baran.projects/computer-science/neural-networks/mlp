import numpy as np


def logarithmic_scale(number, low, high, scale=1., revert=False, shuffle=False):
    samples = np.arange(0, number) * scale
    samples = np.exp(-samples)
    samples = (1 - samples) if revert else samples
    samples = samples * (high - low) + low
    if shuffle:
        np.random.shuffle(samples)
    return samples


def linear_scale(number, low, high, shuffle=False):
    samples = np.linspace(low, high, number)
    if shuffle:
        np.random.shuffle(samples)
    return samples
