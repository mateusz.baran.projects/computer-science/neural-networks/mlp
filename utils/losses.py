import numpy as np


class LossFunction:
    activations = {}

    def __call__(self, y, y_pred):
        raise NotImplementedError

    def check_activation(self, activation):
        raise NotImplementedError


class MeanSquaredError(LossFunction):
    def __call__(self, y, y_pred):
        error = y - y_pred
        cost = np.mean(np.square(error))
        error = error / y.shape[0]

        return error, cost

    def check_activation(self, activation):
        return activation.name in ('sigmoid', 'relu', 'tanh', 'identity', 'softmax')


class CrossEntropy(LossFunction):
    def __call__(self, y, y_pred, epsilon=1e-10):
        error = (y - y_pred) / (y_pred * (1 - y_pred) * y.shape[0] + epsilon)
        y_pred = np.clip(y_pred, epsilon, 1 - epsilon)
        cost = np.mean(-(y * np.log(y_pred) + (1 - y) * np.log(1 - y_pred)))
        return error, cost

    def check_activation(self, activation):
        return activation.name in ('sigmoid', 'softmax')


class SoftmaxCrossEntropy(LossFunction):
    def __call__(self, y, y_pred):
        y = np.argmax(y, axis=1)
        m = y.shape[0]

        exps = np.exp(y_pred - np.max(y_pred))
        y_pred = exps / np.sum(exps, axis=1, keepdims=True)

        log_likelihood = -np.log(y_pred[range(m), y])
        cost = np.mean(log_likelihood)

        error = np.array(y_pred)
        error[range(m), y] -= 1
        error = -error / m

        return error, cost

    def check_activation(self, activation):
        return activation.name == 'identity'
