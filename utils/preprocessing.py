import numpy as np


def convert_to_one_hot(labels):
    n_values = np.max(labels) + 1
    return np.eye(n_values)[labels]


def normalize_and_flatten(features):
    shape = features.shape[1:]
    pixels = 1
    for size in shape:
        pixels *= size

    flatten = np.reshape(features, (-1, pixels))
    normalized = flatten / 255
    return normalized


def normalize_and_reshape(features, shape):
    flatten = np.reshape(features, (-1, *shape))
    normalized = flatten / 255
    return normalized.astype(np.float16)
