import numpy as np


class Initializer:
    def __call__(self, shape):
        raise NotImplementedError


def _compute_fans(shape):
    if len(shape) == 2:
        fan_in = shape[0]
        fan_out = shape[1]
    elif len(shape) == 4:
        receptive_field_size = np.prod(shape[:-2])
        fan_in = shape[-2] * receptive_field_size
        fan_out = shape[-1] * receptive_field_size
    else:
        fan_in = np.sqrt(np.prod(shape))
        fan_out = np.sqrt(np.prod(shape))
    return fan_in, fan_out


class XavierInitializer(Initializer):
    def __call__(self, shape):
        fan_in, fan_out = _compute_fans(shape)
        r = np.sqrt(6 / (fan_in + fan_out))
        weights = np.random.uniform(-r, r, size=shape)
        return weights


class ZeroInitializer(Initializer):
    def __call__(self, shape):
        weights = np.zeros(shape=shape)
        return weights


class NormalInitializer(Initializer):
    def __init__(self, loc, scale, a):
        self.loc = loc
        self.scale = scale
        self.a = a

    def __call__(self, shape):
        fan_in, fan_out = _compute_fans(shape)
        weights = np.random.normal(self.loc, self.scale, size=shape)
        weights = weights * np.sqrt(self.a / fan_out)
        return weights
