from collections import defaultdict

import numpy as np


class Optimizer:
    def __call__(self, name, grads):
        raise NotImplementedError


class SGD(Optimizer):
    def __init__(self, learning_rate=0.1):
        self.learning_rate = learning_rate

    def __call__(self, name, grads):
        delta_grads = self.learning_rate * grads
        return delta_grads


class Momentum(Optimizer):
    def __init__(self, learning_rate=0.01, momentum=0.9):
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.velocity = defaultdict(int)

    def __call__(self, name, grads):
        self.velocity[name] = self.momentum * self.velocity[name] + self.learning_rate * grads
        delta_grads = self.velocity[name]
        return delta_grads


class NesterovMomentum(Optimizer):
    def __init__(self, learning_rate=0.01, momentum=0.9):
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.velocity = defaultdict(int)

    def __call__(self, name, grads):
        prev_velocity = self.velocity[name]
        self.velocity[name] = self.momentum * self.velocity[name] + self.learning_rate * grads
        delta_grads = -self.momentum * prev_velocity + (1 + self.momentum) * self.velocity[name]
        return delta_grads


class Adam(Optimizer):
    def __init__(self, learning_rate=0.001, beta1=0.9, beta2=0.999, epsilon=1e-8):
        self.learning_rate = learning_rate
        self.beta1 = beta1
        self.beta2 = beta2
        self.epsilon = epsilon
        self.m = defaultdict(int)
        self.v = defaultdict(int)
        self.t = defaultdict(int)

    def __call__(self, name, grads):
        self.m[name] = self.beta1 * self.m[name] + (1 - self.beta1) * grads
        self.v[name] = self.beta2 * self.v[name] + (1 - self.beta2) * grads ** 2

        self.t[name] += 1
        m_hat = self.m[name] / (1 - self.beta1 ** self.t[name])
        v_hat = self.v[name] / (1 - self.beta2 ** self.t[name])

        delta_grads = self.learning_rate / (np.sqrt(v_hat) + self.epsilon) * m_hat
        return delta_grads


class AdaGrad(Optimizer):
    def __init__(self, learning_rate=0.01, epsilon=1e-8):
        self.learning_rate = learning_rate
        self.epsilon = epsilon
        self.G = defaultdict(int)

    def __call__(self, name, grads):
        self.G[name] = self.G[name] + grads ** 2

        delta_grads = self.learning_rate / np.sqrt(self.G[name] + self.epsilon) * grads
        return delta_grads


class AdaDelta(Optimizer):
    def __init__(self, rho=0.95, epsilon=1e-8):
        self.rho = rho
        self.epsilon = epsilon
        self.E = defaultdict(int)
        self.Ed = defaultdict(int)

    def __call__(self, name, grads):
        self.E[name] = self.rho * self.E[name] + (1 - self.rho) * grads ** 2
        rms = np.sqrt(self.Ed[name] + self.epsilon)

        delta_grads = rms / np.sqrt(self.E[name] + self.epsilon) * grads

        self.Ed[name] = self.rho * self.Ed[name] + (1 - self.rho) * delta_grads ** 2

        return delta_grads
