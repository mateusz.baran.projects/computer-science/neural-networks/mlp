import numpy as np


class Activation:
    name = 'activation'

    def __init__(self):
        self.layer_in = np.array([])
        self.layer_out = np.array([])

    def __call__(self, z):
        raise NotImplementedError

    def backward(self, error):
        raise NotImplementedError


class Sigmoid(Activation):
    name = 'sigmoid'

    def __call__(self, z):
        self.layer_in = z
        self.layer_out = 1 / (1 + np.exp(-z))
        return self.layer_out

    def backward(self, error):
        return self.layer_out * (1 - self.layer_out) * error


class ReLU(Activation):
    name = 'relu'

    def __call__(self, z):
        self.layer_in = z
        self.layer_out = np.where(z > 0, z, 0)
        return self.layer_out

    def backward(self, error):
        return np.where(self.layer_out > 0, 1, 0) * error


class Tanh(Activation):
    name = 'tanh'

    def __call__(self, z):
        self.layer_in = z
        self.layer_out = np.tanh(z)
        return self.layer_out

    def backward(self, error):
        return (1 - np.tanh(self.layer_out) ** 2) * error


class Identity(Activation):
    name = 'identity'

    def __call__(self, z):
        self.layer_in = z
        self.layer_out = z
        return self.layer_out

    def backward(self, error):
        return error


class Softmax(Activation):
    name = 'softmax'

    def __call__(self, z):
        self.layer_in = z
        exps = np.exp(z - np.max(z))
        self.layer_out = exps / np.sum(exps, axis=1, keepdims=True)
        return self.layer_out

    def backward(self, error):
        expanded = np.expand_dims(self.layer_out, axis=1)
        diag = expanded * np.eye(self.layer_out.shape[1])
        jac = diag - np.einsum('mij,mkj->mik', diag, diag)
        return np.einsum('mj,mkj->mk', error, jac)
