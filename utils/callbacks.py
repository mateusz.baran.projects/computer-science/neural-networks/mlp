class Callback:
    def on_batch_begin(self, model):
        pass

    def on_batch_end(self, model):
        pass

    def on_epoch_begin(self, model):
        pass

    def on_epoch_end(self, model):
        pass


class SaveBestValAccuracyCallback(Callback):
    def __init__(self, path):
        self.path = path
        self.best_accuracy = 0

    def on_epoch_end(self, model):
        if model.epoch_history['val_accuracy'][-1] > self.best_accuracy:
            self.best_accuracy = model.epoch_history['val_accuracy'][-1]
            model.save_model(self.path)


class MetricPrintCallback(Callback):
    def on_batch_end(self, model):
        log = '| '
        for metric, measures in model.batch_history.items():
            if isinstance(measures[-1], float):
                log += f'{metric}: {measures[-1]:.5f} | '
            else:
                log += f'{metric}: {measures[-1]} | '

        print(f'\r{log}', end='')

    def on_epoch_end(self, model):
        log = '| '
        for metric, measures in model.epoch_history.items():
            if isinstance(measures[-1], float):
                log += f'{metric}: {measures[-1]:.5f} | '
            else:
                log += f'{metric}: {measures[-1]} | '

        print(f'\r{log}')
