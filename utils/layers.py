import numpy as np

from utils.activations import Identity
from utils.initializers import XavierInitializer, ZeroInitializer


class Layer:
    def __init__(self, name):
        self.name = name
        self.layer_in = np.array([])
        self.layer_out = np.array([])

    def __call__(self, input_shape, optimizer, calc_error):
        raise NotImplementedError

    def forward(self, layer_in):
        raise NotImplementedError

    def backward(self, error):
        raise NotImplementedError


class Dense(Layer):
    def __init__(self, size, activation=None, initializer=None, bias_initializer=None,
                 use_bias=True, name='Dense'):
        super().__init__(name)
        self.size = size
        self.use_bias = use_bias
        self.activation = activation or Identity()
        self.initializer = initializer or XavierInitializer()
        self.bias_initializer = bias_initializer or ZeroInitializer()

    def __call__(self, input_shape, optimizer, calc_error):
        self.weights = self.initializer((input_shape[0], self.size))
        self.biases = self.bias_initializer((1, self.size))
        self.optimizer = optimizer
        self.calc_error = calc_error
        return self.size,

    def forward(self, layer_in):
        self.layer_in = layer_in

        layer_out = layer_in @ self.weights
        if self.use_bias:
            layer_out += self.biases

        self.layer_out = self.activation(layer_out)
        return self.layer_out

    def backward(self, error):
        d_layer = self.activation.backward(error)

        if self.calc_error:
            error = d_layer @ self.weights.T

        self.weights += self.optimizer(id(self.weights), self.layer_in.T @ d_layer)
        if self.use_bias:
            self.biases += self.optimizer(id(self.biases), np.sum(d_layer, axis=0, keepdims=True))
        return error


def _compute_output_shape(input_size, kernel_size, pad_size, stride_size):
    return int((input_size - kernel_size + 2 * pad_size) / stride_size) + 1


def _padding(layer, h_pad, w_pad):
    return np.pad(layer, ((0, 0), (h_pad, h_pad), (w_pad, w_pad), (0, 0)), 'constant',
                  constant_values=0)


def _padding_remove(layer, h_pad, w_pad):
    if h_pad:
        return layer[:, h_pad:-h_pad]
    if w_pad:
        return layer[:, :, w_pad:-w_pad]
    return layer


class Convolution2D(Layer):
    def __init__(self, filters, kernel, stride=(1, 1), pad=(0, 0), activation=None,
                 kernel_initializer=None, bias_initializer=None, use_bias=True,
                 name='Convolution2D'):
        super().__init__(name)
        self.filters = filters
        self.kernel = kernel
        self.stride = stride
        self.pad = pad
        self.use_bias = use_bias
        self.activation = activation or Identity()
        self.initializer = kernel_initializer or XavierInitializer()
        self.bias_initializer = bias_initializer or ZeroInitializer()

    def __call__(self, input_shape, optimizer, calc_error):
        shape = (self.kernel[0], self.kernel[1], input_shape[2], self.filters)

        self.weights = self.initializer(shape)
        self.biases = self.bias_initializer((1, 1, 1, self.filters))
        self.calc_error = calc_error
        self.optimizer = optimizer

        nh = _compute_output_shape(input_shape[0], self.kernel[0], self.pad[0], self.stride[0])
        nw = _compute_output_shape(input_shape[1], self.kernel[1], self.pad[1], self.stride[1])

        self.input_shape = input_shape
        self.output_shape = (nh, nw, self.filters)
        return self.output_shape

    def forward(self, layer_in):
        self.layer_in = layer_in
        layer_in = _padding(layer_in, self.pad[0], self.pad[1])

        expanded_layer_in = np.lib.stride_tricks.as_strided(
            layer_in,
            shape=(
                layer_in.shape[0],
                self.output_shape[0],
                self.output_shape[1],
                self.weights.shape[0],
                self.weights.shape[1],
                self.weights.shape[2],
            ),
            strides=(
                layer_in.strides[0],
                layer_in.strides[1] * self.stride[0],
                layer_in.strides[2] * self.stride[1],
                layer_in.strides[1],
                layer_in.strides[2],
                layer_in.strides[3],
            ),
            writeable=False,
        )

        layer_out = np.einsum('MHWhwc,hwcf->MHWf', expanded_layer_in, self.weights)
        if self.use_bias:
            layer_out += self.biases

        self.layer_out = self.activation(layer_out)
        return self.layer_out

    def backward(self, error):
        d_layer = self.activation.backward(error)

        d_layer = np.insert(d_layer, np.repeat(np.arange(1, d_layer.shape[1]), self.stride[0] - 1),
                            values=0, axis=1)
        d_layer = np.insert(d_layer, np.repeat(np.arange(1, d_layer.shape[2]), self.stride[1] - 1),
                            values=0, axis=2)

        if self.calc_error:
            weight = np.rot90(self.weights, 2, axes=(0, 1))
            d_layer_pad = _padding(d_layer, weight.shape[0] - 1, weight.shape[1] - 1)

            expanded_d_layer_pad = np.lib.stride_tricks.as_strided(
                d_layer_pad,
                shape=(
                    d_layer_pad.shape[0],
                    self.input_shape[0],
                    self.input_shape[1],
                    weight.shape[0],
                    weight.shape[1],
                    weight.shape[3],
                ),
                strides=(
                    d_layer_pad.strides[0],
                    d_layer_pad.strides[1],
                    d_layer_pad.strides[2],
                    d_layer_pad.strides[1],
                    d_layer_pad.strides[2],
                    d_layer_pad.strides[3],
                ),
                writeable=False,
            )

            error = np.einsum('MHWhwf,hwcf->MHWc', expanded_d_layer_pad, weight)
            error = _padding_remove(error, self.pad[0], self.pad[1])

        layer_in_pad = _padding(self.layer_in, self.pad[0], self.pad[1])

        expanded_layer_in_pad = np.lib.stride_tricks.as_strided(
            layer_in_pad,
            shape=(
                self.weights.shape[0],
                self.weights.shape[1],
                self.weights.shape[2],
                d_layer.shape[0],
                d_layer.shape[1],
                d_layer.shape[2],
            ),
            strides=(
                layer_in_pad.strides[1],
                layer_in_pad.strides[2],
                layer_in_pad.strides[3],
                layer_in_pad.strides[0],
                layer_in_pad.strides[1],
                layer_in_pad.strides[2],
            ),
            writeable=False,
        )

        d_weights = np.einsum('HWcMhw,Mhwf->HWcf', expanded_layer_in_pad, d_layer)
        self.weights += self.optimizer(id(self.weights), d_weights)
        if self.use_bias:
            d_biases = np.sum(d_layer, axis=(0, 1, 2), keepdims=True)
            self.biases += self.optimizer(id(self.biases), d_biases)

        return error


class Flatten(Layer):
    def __init__(self, name='flatten'):
        super().__init__(name)

    def __call__(self, input_shape, optimizer, calc_error):
        self.input_shape = input_shape
        self.output_shape = np.product(input_shape),
        return self.output_shape

    def forward(self, layer_in):
        layer_out = layer_in.reshape(-1, *self.output_shape)
        return layer_out

    def backward(self, error):
        return error.reshape(-1, *self.input_shape)


class Pooling2D(Layer):
    def __init__(self, pool_size, stride, pad, name):
        super().__init__(name)
        self.pool_size = pool_size
        self.stride = stride
        self.pad = pad
        self.mask = np.array([])

    def __call__(self, input_shape, optimizer, calc_error):
        nh = _compute_output_shape(input_shape[0], self.pool_size[0], self.pad[0], self.stride[0])
        nw = _compute_output_shape(input_shape[1], self.pool_size[1], self.pad[1], self.stride[1])

        self.error_shape = (-1, nh, nw, 1, 1, input_shape[2])
        self.output_shape = (nh, nw, input_shape[2])
        return self.output_shape

    def forward(self, layer_in):
        self.layer_in = layer_in
        layer_in_pad = _padding(layer_in, self.pad[0], self.pad[1])

        expanded_layer_in_pad = np.lib.stride_tricks.as_strided(
            layer_in_pad,
            shape=(
                layer_in_pad.shape[0],
                self.output_shape[0],
                self.output_shape[1],
                self.pool_size[0],
                self.pool_size[1],
                self.output_shape[2],
            ),
            strides=(
                layer_in_pad.strides[0],
                layer_in_pad.strides[1] * self.stride[0],
                layer_in_pad.strides[2] * self.stride[1],
                layer_in_pad.strides[1],
                layer_in_pad.strides[2],
                layer_in_pad.strides[3],
            ),
            writeable=False,
        )

        self._pooling(expanded_layer_in_pad)

        return self.layer_out

    def backward(self, error):
        mask = self.mask * error.reshape(self.error_shape)

        error = np.zeros_like(self.layer_in)
        error = _padding(error, self.pad[0], self.pad[1])

        expanded_error = np.lib.stride_tricks.as_strided(
            error,
            shape=(
                error.shape[0],
                self.output_shape[0],
                self.output_shape[1],
                self.pool_size[0],
                self.pool_size[1],
                self.output_shape[2],
            ),
            strides=(
                error.strides[0],
                error.strides[1] * self.stride[0],
                error.strides[2] * self.stride[1],
                error.strides[1],
                error.strides[2],
                error.strides[3],
            ),
            writeable=True,
        )

        np.add.at(expanded_error, (), mask)

        error = _padding_remove(error, self.pad[0], self.pad[1])

        return error

    def _pooling(self, expanded_layer):
        raise NotImplementedError


class MaxPooling2D(Pooling2D):
    def __init__(self, pool_size=(2, 2), stride=(2, 2), pad=(0, 0), name='MaxPooling2D'):
        super().__init__(pool_size, stride, pad, name)

    def _pooling(self, expanded_layer):
        layer = np.max(expanded_layer, axis=(-3, -2), keepdims=True)
        layer_ = (layer + np.random.uniform(0, 1e-8, size=expanded_layer.shape))
        self.mask = (layer_ == np.max(layer_, axis=(-3, -2), keepdims=True))
        self.layer_out = np.squeeze(layer, axis=(-3, -2))


class AveragePooling2D(Pooling2D):
    def __init__(self, pool_size=(2, 2), stride=(2, 2), pad=(0, 0), name='AveragePooling2D'):
        super().__init__(pool_size, stride, pad, name)

    def _pooling(self, expanded_layer):
        layer = np.mean(expanded_layer, axis=(-3, -2), keepdims=True)
        self.mask = np.ones_like(expanded_layer) / (self.pool_size[0] * self.pool_size[1])
        self.layer_out = np.squeeze(layer, axis=(-3, -2))
