import pickle as pkl
from collections import defaultdict

import numpy as np


class Model:
    def __init__(self, optimizer, loss, layers=None, metrics=None, callbacks=None, verbose=True):
        self.optimizer = optimizer
        self.loss = loss
        self.layers = layers or []
        self.metrics = metrics or []
        self.callbacks = callbacks or []
        self.verbose = verbose
        self.epoch_history = defaultdict(list)
        self.batch_history = defaultdict(list)
        self.is_compiled = False
        self.training = True

    @staticmethod
    def load_model(path):
        with open(path, 'rb') as file:
            return pkl.load(file)

    def save_model(self, path):
        with open(path, 'wb') as file:
            pkl.dump(self, file)

    def predict(self, x):
        for l in self.layers:
            x = l.forward(x)
        return x

    def fit(self, x_train, y_train, validation_data=None, epochs=1000, batch_size=32):
        self._compile(x_train, y_train)

        for epoch in range(1, epochs + 1):
            for callback in self.callbacks:
                callback.on_epoch_begin(self)

            if not self.training:
                break

            m = x_train.shape[0]
            p = np.random.permutation(m)
            x_train = x_train[p]
            y_train = y_train[p]

            self._fit(epoch, x_train, y_train, m, batch_size)

            self._validate(epoch, validation_data, batch_size)

            for callback in self.callbacks:
                callback.on_epoch_end(self)

        return self.epoch_history

    def _compile(self, x, y):
        if not self.is_compiled:
            assert len(self.layers)

            shape = x.shape[1:]
            for i, layer in enumerate(self.layers):
                shape = layer(shape, self.optimizer, calc_error=(i != 0))

            assert y.shape[1] == self.layers[-1].size
            assert self.loss.check_activation(self.layers[-1].activation)

            self.is_compiled = True

    def _fit(self, epoch, x_train, y_train, m, batch_size):
        costs = []
        predicts = []

        for b in range(0, m, batch_size):
            for callback in self.callbacks:
                callback.on_batch_begin(self)

            x = x_train[b:b + batch_size]
            y = y_train[b:b + batch_size]

            y_pred = self.predict(x)
            error, cost = self.loss(y, y_pred)

            for l in reversed(self.layers):
                error = l.backward(error)

            predicts.extend(y_pred)
            costs.append(cost)

            self._record_log(self.batch_history, epoch - 1 + b / m, cost, y, y_pred)

            for callback in self.callbacks:
                callback.on_batch_end(self)

        self._record_log(self.epoch_history, epoch, np.mean(costs), y_train[:m], predicts)

    def _validate(self, epoch, validation, batch_size):
        if validation is not None:
            costs = []
            predicts = []

            x_val, y_val = validation
            m = y_val.shape[0] - y_val.shape[0] % batch_size

            for b in range(0, m, batch_size):
                x = x_val[b:b + batch_size]
                y = y_val[b:b + batch_size]

                y_pred = self.predict(x)
                error, cost = self.loss(y, y_pred)

                predicts.extend(y_pred)
                costs.append(cost)

            self._record_log(self.epoch_history, epoch, np.mean(costs), y_val[:m], predicts, 'val_')

    def _record_log(self, history, epoch, cost, y, y_pred, prefix=''):
        history[prefix + 'epoch'].append(epoch)
        history[prefix + 'cost'].append(cost)
        for metric in self.metrics:
            measure = metric(y, y_pred)
            history[prefix + metric.name].append(measure)
