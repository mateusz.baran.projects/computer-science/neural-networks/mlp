from collections import defaultdict
from time import time

import numpy as np

from dataset import X_train, Y_train, Y_val, X_val
from model import Model
from utils.activations import ReLU, Sigmoid, Softmax
from utils.initializers import NormalInitializer, XavierInitializer, ZeroInitializer
from utils.layers import Dense
from utils.losses import CrossEntropy
from utils.metrics import Accuracy
from utils.optimizers import (SGD, Momentum, Adam, AdaGrad, AdaDelta, NesterovMomentum)
from utils.visualization import (val_accuracy_of_epoch, show_plots, time_of_experiment,
                                 accuracy_and_val_accuracy_of_epoch)


def run_experiment(test_generator):
    results_dict = defaultdict(list)

    for i, (model_dict, train_dict, key, value) in enumerate(test_generator(), 1):
        model = Model(**model_dict)
        label = f'{key}={value}'
        print(f'{i}. {label}')
        start = time()
        history = model.fit(**train_dict)
        stop = time() - start
        results_dict['model_dict'].append(model_dict)
        results_dict['train_dict'].append(train_dict)
        results_dict['time'].append(stop)
        results_dict['label'].append(label)
        for k, v in history.items():
            results_dict[k].append(v)
        print()

    return results_dict


def optimizer_tuning(model_dict, train_dict):
    def generator():
        for optimizer, name in [
            (SGD(learning_rate=0.1), 'sgd, lr=0.1'),
            (SGD(learning_rate=0.01), 'sgd, lr=0.01'),
            (AdaDelta(rho=0.95), 'adadelta, rho=0.95'),
            (AdaGrad(learning_rate=0.01), 'adagrad, rho=0.01'),
            (Momentum(learning_rate=0.01, momentum=0.9), 'momentum, lr=0.01'),
            (NesterovMomentum(learning_rate=0.01), 'nesterov, lr=0.01'),
            (Adam(learning_rate=0.001), 'adam, lr=0.001'),
        ]:
            model_dict['optimizer'] = optimizer
            yield model_dict, train_dict, 'optimizer', name

    return generator


def batch_size_tuning(model_dict, train_dict):
    def generator():
        for batch_size in [10000, 1000, 128, 32, 5]:
            train_dict['batch_size'] = batch_size
            yield model_dict, train_dict, 'batch_size', batch_size

    return generator


def initializer_tuning(model_dict, train_dict):
    def generator():
        for layers, initializer in [
            ([
                 Dense(50, activation=Sigmoid(), initializer=XavierInitializer()),
                 Dense(10, activation=Softmax(), initializer=XavierInitializer()),
             ], 'xavier'),
            ([
                 Dense(50, activation=Sigmoid(), initializer=ZeroInitializer()),
                 Dense(10, activation=Softmax(), initializer=ZeroInitializer()),
             ], 'zero'),
            ([
                 Dense(50, activation=Sigmoid(), initializer=NormalInitializer(0, 1, 2)),
                 Dense(10, activation=Softmax(), initializer=NormalInitializer(0, 1, 2)),
             ], 'normal'),
            ([
                 Dense(50, activation=Sigmoid(), initializer=NormalInitializer(0, 1, 50)),
                 Dense(10, activation=Softmax(), initializer=NormalInitializer(0, 1, 10)),
             ], 'random'),
        ]:
            model_dict['layers'] = layers
            yield model_dict, train_dict, 'initializer', initializer

    return generator


def hidden_layer_size_tuning(model_dict, train_dict):
    def generator():
        for layers, size in [
            ([
                 Dense(10, activation=Sigmoid(), initializer=XavierInitializer()),
                 Dense(10, activation=Softmax(), initializer=XavierInitializer()),
             ], 10),
            ([
                 Dense(50, activation=Sigmoid(), initializer=XavierInitializer()),
                 Dense(10, activation=Softmax(), initializer=XavierInitializer()),
             ], 50),
            ([
                 Dense(100, activation=Sigmoid(), initializer=XavierInitializer()),
                 Dense(10, activation=Softmax(), initializer=XavierInitializer()),
             ], 100),
        ]:
            model_dict['layers'] = layers
            train_dict['epochs'] = 100
            yield model_dict, train_dict, 'size', size

    return generator


def relu_vs_sigmoid(model_dict, train_dict):
    def generator():
        for layers, activation in [
            ([
                 Dense(50, activation=ReLU(), initializer=XavierInitializer()),
                 Dense(10, activation=Softmax(), initializer=XavierInitializer()),
             ], "relu"),
            ([
                 Dense(50, activation=Sigmoid(), initializer=XavierInitializer()),
                 Dense(10, activation=Softmax(), initializer=XavierInitializer()),
             ], "sigmoid"),
        ]:
            model_dict['layers'] = layers
            yield model_dict, train_dict, 'activation', activation

    return generator


if __name__ == '__main__':
    np.random.seed(3)

    model_dictionary = {
        'optimizer': SGD(learning_rate=0.01),
        'loss': CrossEntropy(),
        'layers': [
            Dense(50, activation=Sigmoid(), initializer=XavierInitializer()),
            Dense(10, activation=Softmax(), initializer=XavierInitializer()),
        ],
        'metrics': [
            Accuracy(),
        ],
    }

    train_dictionary = {
        'x_train': X_train,
        'y_train': Y_train,
        'validation': (X_val, Y_val),
        'epochs': 30,
        'batch_size': 32,
    }

    # Experiment 1
    results = run_experiment(optimizer_tuning(model_dictionary, train_dictionary))
    val_accuracy_of_epoch(results, 'optimizer tuning')
    time_of_experiment(results, 'optimizer tuning time')

    # Experiment 2
    results = run_experiment(batch_size_tuning(model_dictionary, train_dictionary))
    val_accuracy_of_epoch(results, 'batch size tuning')
    time_of_experiment(results, 'batch size tuning time')

    # Experiment 3
    results = run_experiment(initializer_tuning(model_dictionary, train_dictionary))
    val_accuracy_of_epoch(results, 'initializer tuning')

    # Experiment 4
    results = run_experiment(hidden_layer_size_tuning(model_dictionary, train_dictionary))
    accuracy_and_val_accuracy_of_epoch(results, 'hidden layer size tuning')

    # Experiment 5
    results = run_experiment(relu_vs_sigmoid(model_dictionary, train_dictionary))
    accuracy_and_val_accuracy_of_epoch(results, 'relu vs sigmoid')

    show_plots()
