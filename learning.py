import numpy as np

from dataset import X_test, Y_test, X_train, Y_train, Y_val, X_val
from model import Model
from utils.activations import ReLU
from utils.callbacks import MetricPrintCallback
from utils.initializers import XavierInitializer
from utils.layers import Dense, Convolution2D, Flatten, MaxPooling2D
from utils.losses import SoftmaxCrossEntropy
from utils.metrics import Accuracy
from utils.optimizers import Adam

np.random.seed(1)
model = Model(
    optimizer=Adam(),
    loss=SoftmaxCrossEntropy(),
    layers=[
        Convolution2D(8, (3, 3), activation=ReLU(), kernel_initializer=XavierInitializer()),
        MaxPooling2D((2, 2), stride=(2, 2)),
        Flatten(),
        Dense(50, activation=ReLU(), initializer=XavierInitializer()),
        Dense(10, activation=None, initializer=XavierInitializer()),
    ],
    metrics=[
        Accuracy(),
    ],
    callbacks=[
        MetricPrintCallback(),
    ],
)

model.fit(
    x_train=X_train,
    y_train=Y_train,
    validation_data=(X_val, Y_val),
    epochs=2,
    batch_size=64,
)

Y_pred = model.predict(X_test)

test_acc = Accuracy()(Y_pred, Y_test)

print('Test accuracy:', test_acc)
