from keras.datasets import mnist

from utils.preprocessing import convert_to_one_hot, normalize_and_reshape

(X_train_orig, Y_train_orig), (X_test_orig, Y_test_orig) = mnist.load_data()

X_train = normalize_and_reshape(X_train_orig[:50000], (28, 28, 1))
Y_train = convert_to_one_hot(Y_train_orig[:50000])

X_val = normalize_and_reshape(X_train_orig[50000:], (28, 28, 1))
Y_val = convert_to_one_hot(Y_train_orig[50000:])

X_test = normalize_and_reshape(X_test_orig, (28, 28, 1))
Y_test = convert_to_one_hot(Y_test_orig)
